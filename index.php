<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8" />
	
	<link rel="stylesheet" href="CSS/bootstrap.min.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">	
	<link rel="stylesheet" href="CSS/main.css" />
	<link rel="stylesheet" href="CSS/axel.css" />
	<link rel="stylesheet" href="CSS/franck.css" />
	<link rel="stylesheet" href="CSS/gregoire.css" />
	<link rel="stylesheet" href="CSS/sauveur.css" />
	<script src="JS/jquery-3.4.1.slim.min.js"></script>	
	<script src="JS/popper.min.js"></script>
	<script src="JS/bootstrap.min.js"></script>
	<script src="JS/main.js"></script>	
	<script src="JS/franck.js"></script>	
	<title>Maquette FNH</title>
</head>
<body>

<?php require "HTML/nav.html" ?>

<?php require "HTML/axel.html" ?>

<?php require "HTML/sauveur.html" ?>

<?php require "HTML/franck.html" ?>

<?php require "HTML/footer.html" ?>


</body>
</html>